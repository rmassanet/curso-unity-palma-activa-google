﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;
using GooglePlayGames.BasicApi;

public class TestGPG : MonoBehaviour {
	public Text debugText;
	public CanvasGroup buttonCanvas;

	void Start ()
	{
		PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
			// enables saving game progress.
//			.EnableSavedGames()
				// registers a callback to handle game invitations received while the game is not running.
				.WithInvitationDelegate(delegate(GooglePlayGames.BasicApi.Multiplayer.Invitation invitation, bool shouldAutoAccept) {
					Debug.Log("Invitation delegate");
				})
				// registers a callback for turn based match notifications received while the
				// game is not running.
				.WithMatchDelegate(delegate(GooglePlayGames.BasicApi.Multiplayer.TurnBasedMatch match, bool shouldAutoLaunch) {
					Debug.Log("Match delegate");
				})
				.Build();
		
		PlayGamesPlatform.InitializeInstance(config);
		// recommended for debugging:
		PlayGamesPlatform.DebugLogEnabled = true;
		// Activate the Google Play Games platform
		PlayGamesPlatform.Activate();
		buttonCanvas.interactable = true;
		buttonCanvas.blocksRaycasts = true;
	}

	public void Login()
	{
		Social.localUser.Authenticate((bool success) => {
			debugText.text = "Authenticate: " + success.ToString();
		});
	}

	public void UnlockAchivement1()
	{
		Social.ReportProgress("CgkI0dzDx-sbEAIQAA", 100.0f, (bool success) => {
			debugText.text = "UnlockAchivement1: " + success.ToString();
		});
	}

	public void IncrementAchievement2()
	{
		PlayGamesPlatform.Instance.IncrementAchievement(
			"CgkI0dzDx-sbEAIQAQ", 5, (bool success) => {
			debugText.text = "IncrementAchievement2: " + success.ToString();
		});
	}

	public void ShowLeaderboards()
	{
		Social.ShowLeaderboardUI();
	}

	public void ReportScore()
	{
		Social.ReportScore (100, "CgkI0dzDx-sbEAIQBQ", (bool success) => {
			debugText.text = "ReportScore: " + success.ToString();
		});
	}
}
