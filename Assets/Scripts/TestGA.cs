﻿using UnityEngine;
using System.Collections;

public class TestGA : MonoBehaviour {

	public GoogleAnalyticsV3 tracker;

	// Use this for initialization
	void Start () {
		tracker.LogScreen ("Main menu");
	}
	
	public void LogEvent()
	{
		tracker.LogEvent ("MainMenuEvent", "ButtonClicked", "SendEvent", 1);
	}
}
