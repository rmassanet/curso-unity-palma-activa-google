﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;

public class TestAdMob : MonoBehaviour {

	InterstitialAd interstitial;

	// Use this for initialization
	void Start () {
	
		interstitial = new InterstitialAd("ca-app-pub-2871385714660803/3319085775");
		// Create an empty ad request.
//		AdRequest request = new AdRequest.Builder().Build();
		AdRequest request = new AdRequest.Builder()
			.AddTestDevice(AdRequest.TestDeviceSimulator)
				.AddTestDevice("468B63478DBFD5D6B4373C9226FD1483")
				.AddKeyword("unity")
				.SetGender(Gender.Male)
				.SetBirthday(new System.DateTime(1985, 1, 1))
				.TagForChildDirectedTreatment(true)
				.AddExtra("color_bg", "9B30FF") // Sets text ad background color.
				.Build();

		// Load the interstitial with the request.
		interstitial.LoadAd(request);
		interstitial.AdLoaded += HandleAdLoaded;

//		BannerView banner = new BannerView ("ca-app-pub-2871385714660803/3319085775", AdSize.Banner, AdPosition.Bottom);


	}

	void HandleAdLoaded (object sender, System.EventArgs e)
	{
		if (interstitial.IsLoaded()) {
			interstitial.Show();
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
